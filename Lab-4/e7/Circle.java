package g30125.naghi.flavia.l4.e7;



public class Circle {
	 double radius;           
     String color;                      
     
     public Circle(){            
   	  this.color="red";            
   	  this.radius=1.0;          
   	  }          
     public Circle(double radius){          
   	  this.radius=1.0;        
   	  }           
     double getRadius(){               
   	  return this.radius;         
   	  }           
     double getArea(){                        
   	  return 3.14159265359*radius*radius;          
   	  }           
     public static void main(String[] args){           
   	  Circle c=new Circle();            
   	  System.out.println("Raza="+c.getRadius());            
   	  System.out.println("Aria="+c.getArea());         
   	  }
}
