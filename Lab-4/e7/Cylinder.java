package g30125.naghi.flavia.l4.e7;

import g30125.naghi.flavia.l4.e3.Circle;

public class Cylinder extends Circle {
	double height;
	
	public Cylinder() {
		this.height=1.0;
	}
	public Cylinder(double radius) {
		super(radius);
		this.getRadius();
	}
	public Cylinder(double height,double radius) {
		super(radius);
		this.height=1.0;
	}
	public double getHeight() {
		return height;
	}
	public double getArea() {
		return 2*3.1415926535*radius*height;
	}

	 public static void main(String[] args){           
   	  Cylinder c1=new Cylinder();  
   	  System.out.println("Raza="+c1.getRadius());
   	  System.out.println("Aria="+c1.getArea()); 
      System.out.println("Inaltimea="+c1.getHeight());
      
	 }
}
