package g30125.naghi.flavia.l4.e3;

public class Circle{
	          protected double radius;           
	          public String color;                      
	         
	          public Circle(){            
	        	  this.color="red";            
	        	  this.radius=1.0;          
	        	  }          
	          public Circle(double radius){          
	        	  this.radius=1.0;        
	        	  }           
	         public double getRadius(){               
	        	  return this.radius;         
	        	  }           
	          public double getArea(){                        
	        	  return 3.14159265359*radius*radius;          
	        	  } 
	     
	          public static void main(String[] args){           
	        	  Circle c=new Circle();            
	        	  System.out.println(c.getRadius());            
	        	  System.out.println(c.getArea());         
	        	  }
	          }

