package g30125.naghi.flavia.l4.e6;

public class Book {
	
	String name;
	Author[] authors;
	double price;
	int qtyInStock=0;
	int k;
	
	public Book(String name,Author[] authors,double price) {
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	public Book(String name,Author[] authors,double price,int qtyInStock) {
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName() {
		return this.name;
	}
	public Author[] getAuthors() {
		k++;
		return this.authors;
	}
	public double getPrice() {
		return this.price;
	}
	void setPrice(double price) {
		this.price=price;
	}
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	void setQtyInStock(int qtyInStock) {
		this.qtyInStock=qtyInStock;
	}
	public String toString() {
		return this.name+" by "+k+" autori";
	}
	void printAuthors() {
		 System.out.println("Alina Miti");
	}
    
}
