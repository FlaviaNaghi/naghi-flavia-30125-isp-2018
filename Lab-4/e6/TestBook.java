package g30125.naghi.flavia.l4.e6;

public class TestBook {
	public static void main(String[] args) {
		
		Book b2=new Book("Padurea",new Author[1],30);
		    System.out.println("Numele cartii:"+b2.getName());
		    System.out.println("Pretul:"+b2.getPrice());
		    System.out.println(b2.getAuthors());
		    System.out.println(b2.toString());
		    
		    Book b3=new Book("Padurea",new Author[1],30,56);
		    System.out.println("Cantitate:"+b3.getQtyInStock());
		    System.out.println("Autorii:");
		    b3.printAuthors();
		    
	}

}