package g30125.naghi.flavia.l4.e8;

public class Circle extends Shape {
	private double radius;
	 public Circle() {
		 this.radius=1.0;
	 }
	 public Circle(double radius) {
		 this.radius=radius;
	 }
	 public Circle(double radius,String color,boolean filled) {
		 this.radius=radius;
		 this.color=color;
		 this.filled=filled;
	 }
	 public double getRadius() {
		 return this.radius;
	 }
	 public void setRadius(double radius) {
		 this.radius=radius;
	 }
	 public double getArea() {
		 return 3.14159265359*radius*radius;
	 }
	 public double getPerimeter() {
		 return 2*3.14159265359*radius;
	 }
	 public String toString() {
		 return "A Circle with radius "+this.radius+" ,which is a subclass of "+super.toString();
	 }
	
	}
	
 

