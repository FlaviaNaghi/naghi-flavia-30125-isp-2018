package g30125.naghi.flavia.l4.e8;

public class TestProgram {
	 public static void main(String[] args) {
	  System.out.println("------Shape-----------");
	  
	  Shape s=new Shape();
	  System.out.println(s.getColor());
	  System.out.println(s.isFilled());
	  System.out.println(s.toString());
	   System.out.println("---");
	  Shape s1=new Shape("red",true);
	  System.out.println(s1.getColor());
	  System.out.println(s1.isFilled());
	  System.out.println(s.toString());
	  
	  System.out.println("------Circle-----------");
	  
	  Circle c3=new Circle();
	  System.out.println("Raza="+c3.getRadius());
  	  System.out.println("Perimetrul="+c3.getPerimeter());
  	  System.out.println("Aria="+c3.getArea());
  	  System.out.println(c3.toString());
  	  
  	 System.out.println("-------Rectangle----------");
  	 
  	
   	  Rectangle r1=new Rectangle();
	  System.out.println("Latimea="+r1.getWidth());
	  System.out.println("Lungimea="+r1.getLength());
	  System.out.println("Perimetrul="+r1.getPerimeter());
	  System.out.println("Aria="+r1.getArea());
	  System.out.println(r1.toString());
	  
	  System.out.println("-------Square----------");
	  
	  Square p1=new Square();
		
  	  System.out.println("Lungimea laturii="+p1.getSide());
  	  System.out.println(p1.toString());

	  
}
	 
}
