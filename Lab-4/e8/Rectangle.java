package g30125.naghi.flavia.l4.e8;

public class Rectangle extends Shape{
	protected static double width;
	protected static double length;

	public Rectangle(){
		this.width=1.0;
		this.length=1.0;
	}
	public Rectangle(double width,double length){
		this.width=width;
		this.length=length;
	}
	public Rectangle(double width,double length,String color,boolean filled){
		this.width=width;
		this.length=length;
		this.color=color;
		this.filled=filled;
	}
	public double getWidth() {
		return this.width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getLength() {
		return this.length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getArea() {
		return length*width;
	}
	public double getPerimeter() {
		return 2*(length+width);
	}
	public String toString() {
		return "A Rectangle with width="+this.width+" and length= "+this.length+",which is a subclass of "
	+super.toString();
	}
	
}
