package g30125.naghi.flavia.l8.e1;

public class CoffeeTest {

    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
              Coffee c = mk.makeCoffee();
              try {
                    d.drinkCoffee(c);
              } catch (TemperatureException e) {
                    System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
              } catch (ConcentrationException e) {
                    System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
              } catch (SugarException e) {
                  System.out.println("Exception:"+e.getMessage()+" number="+e.getSugar());
            }
              finally{
                    System.out.println("Throw the cofee cup.\n");
              }
        }    
  }
}//.class
