package g30125.naghi.flavia.l8.e1;

public class CoffeeDrinker {

    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, SugarException{
        if(c.getTemp()>60)
              throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
              throw new ConcentrationException(c.getConc(),"Cofee concentration to high!"); 
        if(c.getSugar()>70)
            throw new SugarException("Too many coffees!",c.getSugar());       
        System.out.println("Drink cofee:"+c);
  }
}//.class
