package g31025.naghi.flavia.l2.e6;

public class Ex6b {
	public static long factorial(int n) { 
	    if (n == 1) return 1; 
	    return n * factorial(n-1); 
	} 

}
