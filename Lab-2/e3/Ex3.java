package g31025.naghi.flavia.l2.e3;

import java.util.Scanner;
public class Ex3 {
	 public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int A = in.nextInt();
	        int B = in.nextInt();

	        int count = 0;

	        // loop through the numbers one by one
	        for (int i = A; i < B; i++) {

	            boolean numarprim = true;

	            // check to see if the number is prime
	            for (int j = 2; j < i; j++) {
	                if (i % j == 0) {
	                    numarprim = false;
	                    break; // exit the inner for loop
	                }
	            }

	            // print the number if prime
	            if (numarprim) {
	                count++;
	                System.out.print(i + ", ");
 
	            }
}
	        System.out.println("Count"+count);
	 }
}


