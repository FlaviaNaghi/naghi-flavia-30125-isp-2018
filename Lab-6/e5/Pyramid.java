package g30125.naghi.flavia.l6.e5;

	import java.awt.Graphics;
	import javax.swing.JFrame;

	public class Pyramid extends JFrame{

		private int width;
		private int height;
		private int nr;
		
		public Pyramid() {	}
		public Pyramid(int widith, int height, int nr) {
			super();
			this.width = widith;
			this.height = height;
			this.nr = nr;
		}
		
		public void draw(Graphics g) {
			int s=0;
	        int nrt=0;
	        int p,poz;
	        while(nrt+s<nr) {
	        	s++;
	        	p=s;
	        	nrt+=s;
	        	while(p>0) {
	        		for(int i=0 ; i<p;i++)
	        			for(int j=0 ; j<=i;j++)
	        			{	
	        				g.drawRect((DrawingBoard.x/2-(p-j*2)*width/2), height*p+40, width, height);
	        			}
	        		p--;
	        	}
	        }
	        
	    }
	}

