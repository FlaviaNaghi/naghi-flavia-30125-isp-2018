package g30125.naghi.flavia.l6.e5;


	import java.awt.Graphics;
	import javax.swing.JFrame;
	import javax.swing.WindowConstants;

	public class DrawingBoard extends JFrame {

		Pyramid[] p= new Pyramid[100];
		public static int x=900;
		public static int y=600;
	  
	    public DrawingBoard() {
	        super();
	        this.setTitle("Drawing pyramid");
	        this.setSize(x,y);
	        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        this.setVisible(true);
	    }

	    public void add(Pyramid p1){
	    	 for(int i=0;i<p.length;i++){
		            if(p[i]==null){
		                p[i] = p1;
		                break;
		            }
		        }
		        this.repaint();
		    }

	    public void paint(Graphics g){
	        for(int i=0;i<p.length;i++){
	            if(p[i]!=null)
	                p[i].draw(g);
	        }
	    }
	    }
