package g30125.naghi.flavia.l6.e3;
import java.awt.*;
public class Circle implements Shape{
	
	    private int radius;
	    
	    private Color color;
	    private int x; //coords
	    private int y;
	    private String id;
	    private boolean filled;

	    public Circle(Color color,int x,int y,String id,boolean filled, int radius) {
	    	this.color = color;
	        this.x=x;
	        this.y=y;
	        this.id=id;
	        this.filled=filled;
	        this.radius = radius;
	    }

	    public int getRadius() {
	        return radius;
	    }

	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a circle "+this.radius+" "+this.color);
	        g.setColor(this.color);
	        g.drawOval(this.x,this.y,radius,radius);
	        if(this.filled)
	        g.fillOval(this.x,this.y, radius, radius);
	    }
	    
	    public String getId() {
	    	return this.id;
	    }
	    
	}

