package g30125.naghi.flavia.l6.e3;
import java.awt.*;

	public interface Shape {

	    public void draw(Graphics g);
	    public String getId();
	}
