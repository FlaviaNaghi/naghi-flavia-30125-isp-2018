package g30125.naghi.flavia.l6.e3;
import java.awt.*;
public class Main {

	/**
	 * @author mihai.hulea
	 */
	    public static void main(String[] args) {
	        DrawingBoard b1 = new DrawingBoard();
	        Shape s1 = new Circle(Color.RED,40,40,"aa1",true, 90);
	        b1.addShape(s1);
	        Shape s2 = new Circle(Color.GREEN,30,30,"34b",false, 100);
	        b1.addShape(s2);
	        Shape s3= new Rectangle(Color.BLUE,50,70,"b5",true,60);
	        b1.addShape(s3);
	        //b1.delete("b5");
	    }
	}

