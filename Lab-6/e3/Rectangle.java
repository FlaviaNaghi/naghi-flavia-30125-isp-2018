package g30125.naghi.flavia.l6.e3;
import java.awt.*;
public class Rectangle implements Shape{

	    private int length;
	    
	    private Color color;
	    private int x; //coords
	    private int y;
	    private String id;
	    private boolean filled;

	    public Rectangle(Color color,int x,int y,String id,boolean filled ,int length) {
	    	this.color = color;
	        this.x=x;
	        this.y=y;
	        this.id=id;
	        this.filled=filled;
	        this.length = length;
	    }

	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a rectangle "+length+" "+this.color);
	        g.setColor(this.color);
	        g.drawRect(this.x,this.y, length, length/2);
	        if(this.filled)
	            g.fillRect(this.x,this.y, length, length/2);
	    }
	    
	    public String getId() {
	    	return this.id;
	    }
	}

