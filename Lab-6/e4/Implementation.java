package g30125.naghi.flavia.l6.e4;
import java.util.Arrays;

	
	public class Implementation implements CharSequence {
		
		private char[] chars;
		
		public Implementation(char[] chars) {
			this.chars=chars;
		}
		
		public char[] returns() {
			return this.chars;
		}
		
		@Override
		public char charAt(int index) {
			if(index<0 && index>chars.length)
				System.exit(0);
			return chars[index];
		}
		 
		@Override
		public int length(){
			return chars.length;
		}
	   
		@Override
		public CharSequence subSequence(int start, int end) {
			if(start<0 && start>chars.length)
				System.exit(0);
			if(end<0 && end>chars.length)
				System.exit(0);
			/*
			char[] s=Arrays.copyOfRange(chars, start, end);
			this.chars=s;
			return null;
			*/
			
			String s=new String();
			for(int i=start;i<end;i++)
				s=s+chars[i];
			return s;
		}
	   
	}

