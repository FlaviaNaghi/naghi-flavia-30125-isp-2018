package g30125.naghi.flavia.l6.e4;

	import static org.junit.Assert.assertEquals;
   import static org.junit.jupiter.api.Assertions.*;

	import java.util.Arrays;

	import org.junit.jupiter.api.Test;

	class ImplementationTest{
		
		char[] array= {'a','b','c','d','e','f','g'};
		Implementation imp=new Implementation(array);
		
		@Test
		void testCharAt() {
			assertEquals(array[2],imp.charAt(2));
		}

		@Test
		void testLength() {
			assertEquals(7,imp.length());
		}

		@Test
		void testSubSequence() {
			assertEquals("de",imp.subSequence(3, 5));
			
		}


	}

