package g30125.naghi.flavia.l5.e2;

public class RotatedImage implements Image{
	private String fileName;
	
	public RotatedImage(String fileName) {
		this.fileName=fileName;
		loadFromDisk(fileName);
	}
	public void display() {
		System.out.println("Display rotated "+fileName);
	}
	private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
}
