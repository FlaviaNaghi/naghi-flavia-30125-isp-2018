package g30125.naghi.flavia.l5.e2;

public class ProxyImage implements Image{
	
	private RealImage realImage;
	private RotatedImage rotatedImage;
	private String fileName;
	  private int choose;
	 
	   public ProxyImage(String fileName,int choose){
	      this.choose=choose;
	      this.fileName=fileName;
	   }
	   

	@Override
	   public void display() {
		   
	         realImage = new RealImage(fileName);
	    	 rotatedImage=new RotatedImage(fileName);
	      
	    	
	   
	     if(choose==1) {
	    	 if(realImage == null){
	             realImage = new RealImage(fileName);
	          }
	          realImage.display();
	    	
	     }
	     else if(choose==2) {
	    	 if(rotatedImage == null) {
	        	  rotatedImage = new RotatedImage(fileName);
	          }
	    	 rotatedImage.display();
	     }
	    	 
	    	  
	   }

}
