package g30125.naghi.flavia.l5.e2;

public class Test {
	public static void main(String[] args) {
		RotatedImage ri=new RotatedImage("74142_l.jpeg");
		ri.display();
		
		RealImage re=new RealImage("3.png");
		re.display();
		
		System.out.println("------------");
		ProxyImage pi=new ProxyImage("74142_l.jpeg",2);
		pi.display();
	}
	

}
