package g30125.naghi.flavia.l5.e1;



public class Circle extends Shape {
	protected double radius;
     
     public Circle(){            
   	          
   	  }          
     public Circle(double radius){          
   	  this.radius=radius;        
   	  } 
     public Circle(double radius,String color,boolean filled) {
    	super(color,filled);
    	this.radius=radius;
    	
     }
    double getRadius(){               
   	  return this.radius;         
   	  }           
    public void setRadius(double radius) {
	       this.radius = radius;
	   }
    public double getArea() {
	       return 3.14*this.radius*this.radius;
	   }

	   public double getPerimeter() {
	       return 2*3.14*this.radius;
	   }

	   public String toString() {
	       return "Circle"+" color=" + this.color+", filled=" +this.filled+", radius=" +this.radius;
	   }
}
