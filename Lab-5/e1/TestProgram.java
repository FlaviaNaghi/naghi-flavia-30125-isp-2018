package g30125.naghi.flavia.l5.e1;

public class TestProgram {
	public static void main(String[] args){
		System.out.println("------Circle------");
		Circle c=new Circle(1.0,"red",true);
		System.out.println(c.toString());
		System.out.println("Aria="+c.getArea());
		System.out.println("Perimetrul="+c.getPerimeter());
		
		
		
		 System.out.println("-------Rectangle----------");
	  	  Rectangle r1=new Rectangle(1.0,1.0,"green",false);
		  System.out.println(r1.toString());
		  System.out.println("Aria="+r1.getArea());
		  System.out.println("Perimetrul="+r1.getPerimeter());
		  
		  
		  
		  
		  System.out.println("-------Square----------");
		  Square p1=new Square(1.0,"yellow",true);
	  	  System.out.println(p1.toString());
		System.out.println("Perimetrul="+p1.getPerimeter());
	}

}



