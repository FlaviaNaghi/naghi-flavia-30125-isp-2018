package g30125.naghi.flavia.l5.e1;

public class Rectangle extends Shape {
	protected  double width;
	protected double length;

	public Rectangle(){
		
	}
	public Rectangle(double width,double length){
		this.width=width;
		this.length=length;
	}
	public Rectangle(double width,double length,String color,boolean filled){
		super(color,filled);
		this.width=width;
		this.length=length;
		this.color=color;
		this.filled=filled;
	}
	public double getWidth() {
		return this.width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getLength() {
		return this.length;
	}
	public void setLength(double length) {
		this.length=length;
	}

	public double getArea() {
		return length*width;
	}
	public double getPerimeter() {
		return 2*(length+width);
	}
	public String toString() {
		return "A Rectangle color "+this.color+" ,filled "+this.filled;
	}

}

