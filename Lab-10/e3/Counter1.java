package g30125.naghi.flavia.l10.e3;


	public class Counter1 extends Thread{

		 
	      Counter1(String name){
	            super(name);
	      }
	 
	      public void run1(){
	            for(int i=0;i<100;i++){
	                  System.out.println(getName() + " i = "+i);
	                  try {
	                        Thread.sleep((int)(Math.random() * 1000));
	                  } catch (InterruptedException e) {
	                        e.printStackTrace();
	                  }
	            }
	            System.out.println(getName() + " job finalised.");
	      }
	      public void run2(){
	            for(int i=100;i<200;i++){
	                  System.out.println(getName() + " i = "+i);
	                  try {
	                        Thread.sleep((int)(Math.random() * 1000));
	                  } catch (InterruptedException e) {
	                        e.printStackTrace();
	                  }
	            }
	            System.out.println(getName() + " job finalised.");
	      }
	 
	      public static void main(String[] args) {
	            Counter1 c1 = new Counter1("counter1");
	            Counter1 c2 = new Counter1("counter2");
	            c1.run1();
	            c2.run2();
	      }
}

	



