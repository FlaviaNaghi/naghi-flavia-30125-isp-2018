import becker.robots.*;

public class ex4
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
    
      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST);
      Wall blockAve5 = new Wall(ny, 2, 2, Direction.EAST);
      Wall blockAve6 = new Wall(ny, 2, 2, Direction.SOUTH);
      Wall blockAve7 = new Wall(ny, 2, 1, Direction.SOUTH);
      Robot karel = new Robot(ny, 0, 2, Direction.WEST);
  
  
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.move();

   }
} 
