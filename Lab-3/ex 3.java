import becker.robots.*;
 
public class DeliverParcel
{
   public static void main(String[] args)
   {  
   	// Set up the initial situation
   	City prague = new City();
      Robot karel = new Robot(prague, 1, 1, Direction.NORTH);
 
		// Direct the robot to the final situation
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();

      }
} 