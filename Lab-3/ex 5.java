import becker.robots.*;
 
public class ex5
{
   public static void main(String[] args)
   {
      
      City toscana = new City();
      Thing parcel = new Thing(ny, 2, 2);
      Wall blockAve0 = new Wall(toscana, 1, 1, Direction.WEST);
      Wall blockAve1 = new Wall(toscana, 2, 1, Direction.WEST);
      Wall blockAve2 = new Wall(toscana, 1, 1, Direction.NORTH);
      Wall blockAve3 = new Wall(toscana, 1, 2, Direction.NORTH);
      Wall blockAve4 = new Wall(toscana, 1, 2, Direction.EAST);
      Wall blockAve5 = new Wall(toscana, 1, 2, Direction.SOUTH);
      Wall blockAve6 = new Wall(toscana, 2, 1, Direction.SOUTH);
      Robot karel = new Robot(toscana, 1, 2, Direction.SOUTH);
  
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.pickThing();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.putThing();
   }
} 